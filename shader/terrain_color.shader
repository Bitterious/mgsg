shader_type spatial;
render_mode async_visible,diffuse_toon,specular_disabled,cull_back;
uniform vec3 albedo_deep = vec3(.01f,.15f,.04f);
uniform vec3 albedo_high = vec3(.025f,.25f,.08f);
uniform float deephigh_limits = 10.0f;
uniform vec3 uv1_scale;
uniform vec3 uv1_offset;
uniform float rim : hint_range(0,1);
uniform float rim_tint : hint_range(0,1);
uniform sampler2D texture_ambient_occlusion : hint_white;
uniform vec4 ao_texture_channel;
uniform float ao_light_affect;

vec3 lerp(vec3 start, vec3 target, float alpha) {
	vec3 lerpDist = target - start;
	return start + (lerpDist*alpha);
}

void vertex() {
	vec3 albedoBase = albedo_deep;
	vec3 albedoHigh = albedo_high;
	if (VERTEX.y > 4.0f) {
		albedoBase = vec3(.006f,.1f,.03f);
		albedoHigh = vec3(.015f,.2f,.06f);
	} else if (VERTEX.y > 1.0f) {
	} else if (VERTEX.y > -2.5f) {
		albedoBase = vec3(.4,.25,.1);
		albedoHigh = vec3(.5,.35,.2);
	} else {
		albedoBase = vec3(.03,.03,.04);
		albedoHigh = vec3(.09,.09,.1);
	}
	float cutMult = (1.0f/deephigh_limits);
	float alpha = (VERTEX.y+deephigh_limits)*cutMult;
	COLOR = vec4(lerp(albedoBase, albedoHigh, alpha), 1.0f);
	UV=UV*uv1_scale.xy+uv1_offset.xy;
}

void fragment() {
	vec2 base_uv = UV;
	ALBEDO = vec3(COLOR.x, COLOR.y, COLOR.z);
	RIM = rim;
	RIM_TINT = rim_tint;
	AO = dot(texture(texture_ambient_occlusion,base_uv),ao_texture_channel);
	AO_LIGHT_AFFECT = ao_light_affect;
	//ALBEDO = finalColor;
}
