shader_type spatial;
render_mode blend_add,cull_back,specular_schlick_ggx,shadows_disabled,async_visible;
uniform vec3 uv1_scale;
uniform vec3 uv1_offset;
uniform float lastTime;
uniform sampler2D texture_normal : hint_normal;

vec2 lerpv2(vec2 start, vec2 end, float alpha){return start + ((end-start)*alpha);}
float lerpf(float start, float end, float alpha){return start + ((end-start)*alpha);}

void vertex() {
	float timeOffset = abs((TIME-float(int(TIME)))-.5);
	COLOR = vec4(0,.1f,.3f,.5f);
	UV = lerpv2(UV, UV+(vec2(-timeOffset, timeOffset*2.0)*.1), .2);
	VERTEX.y = lerpf(VERTEX.y, VERTEX.y+timeOffset, timeOffset*.1);
}

void fragment() {
	vec2 base_uv = UV;
	ALBEDO = vec3(COLOR.x, COLOR.y, COLOR.z);
	ALPHA = COLOR.a;
	NORMALMAP = texture(texture_normal,base_uv).rgb;
	NORMALMAP_DEPTH = 1.0;
	ROUGHNESS = .2;
	SPECULAR = 1.0;
	METALLIC = 1.0;
	vec2 clearcoat_tex = texture(texture_normal,base_uv).xy;
	CLEARCOAT = clearcoat_tex.x;
	CLEARCOAT_GLOSS = clearcoat_tex.y;
}