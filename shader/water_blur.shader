shader_type spatial;

void fragment() {
	vec3 color = texture(SCREEN_TEXTURE, SCREEN_UV, 5).xyz;
	ALBEDO = color;
}