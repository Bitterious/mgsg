extends Node

var CurrentCamera:Camera = null
var CurrentMap:String = "WORLD1"
var ControlledPlayer:Spatial = null
var RenderDistance:int = 16
var WorldSeed:int = int(Time.get_unix_time_from_system())
var GenThreads:int = 4
var DelThreads:int = 4
