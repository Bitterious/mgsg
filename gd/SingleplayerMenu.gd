extends Control

onready var worldname = $CreateWorld/WorldName

func _create_world():
	var worldname2use = worldname.text
	if not worldname2use: worldname2use = "unnamed"
	var moddedname = worldname2use
	var tryId = 1
	while Directory.new().dir_exists("user://worlds/"+moddedname):
		moddedname = worldname2use+" ("+str(tryId)+")"
		tryId += 1
		pass
	ScriptShared.CurrentMap = moddedname
	var _u = get_tree().change_scene("res://tscn/RandomWorld.tscn")
