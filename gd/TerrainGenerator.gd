extends Spatial

var noise = OpenSimplexNoise.new()
var cnoise = OpenSimplexNoise.new()
var snoise = OpenSimplexNoise.new()
var shaderMat = ShaderMaterial.new()
var waterMesh = MeshInstance.new()
var plrToSpawn = preload("res://tscn/ControlledPlayer.tscn")
var culledVCMat = preload("res://shader/terrain_color.shader")
var culledWCMat = preload("res://material/watershader.material")
var nerdfont = preload("res://theme/nerdfont.theme")
onready var terrain = $Terrain
onready var queuedForDeletion = $TerrainQueued
onready var sun = $Sun
onready var env = $WorldEnvironment
var isActive = true

func _write_file(filename:String, data):
	var filehandle = File.new()
	filehandle.open_compressed(filename, File.WRITE, File.COMPRESSION_GZIP)
	if filehandle.get_error() != OK: print(filehandle.get_error())
	filehandle.store_var(data)
	filehandle.flush()
	filehandle.close()
func _read_file(filename:String):
	var filehandle = File.new()
	filehandle.open_compressed(filename, File.READ)
	if filehandle.get_error() != OK: print(filehandle.get_error())
	var data = filehandle.get_var()
	filehandle.close()
	return data
func _file_exists(filename:String): return File.new().file_exists(filename)

var noiseAmplitude = 22
var waterHeight = -10
func _generate_chunk_mesh(data: Array):
	if !data: return
	var offset = data[0]
	if offset == null: return
	var chunk_node = terrain.get_node_or_null(str(offset))
	if chunk_node != null: return
	var filename = "user://worlds/" + ScriptShared.CurrentMap + "/chunks/" + str(offset) + ".chunk"
	var finalMesh: ArrayMesh = null
	var needWater: bool = false
	var newChunk = _generate_empty_chunk(offset)
	if newChunk == null: return
	if _file_exists(filename):
		finalMesh = _load_mesh_from_file(filename)
	else:
		var meshData = _generate_mesh(offset)
		finalMesh = meshData[0]
		needWater = meshData[1]
	if !isActive: return null
	if needWater:
		newChunk.call_deferred("add_child", waterMesh.duplicate())
	var chunkMesh = _create_mesh_instance(finalMesh)
	newChunk.call_deferred("add_child", waterMesh.duplicate(0))
	newChunk.call_deferred("add_child", chunkMesh)
	chunkMesh.call_deferred("create_trimesh_collision")
	chunkMesh.material_override = shaderMat

func _generate_empty_chunk(offset: Vector2) -> StaticBody:
	if !isActive: return null
	var newChunk = StaticBody.new()
	newChunk.name = str(offset)
	newChunk.translation = Vector3(offset.x, 0, offset.y)
	terrain.call_deferred("add_child", newChunk)
	return newChunk

func _load_mesh_from_file(filename: String) -> ArrayMesh:
	var meshbyte = _read_file(filename)
	var arraymesh = ArrayMesh.new()
	arraymesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, meshbyte)
	return arraymesh

func _generate_mesh(offset: Vector2) -> Array:
	var chunkPlane = PlaneMesh.new()
	chunkPlane.size = Vector2(16, 16)
	chunkPlane.subdivide_depth = 8
	chunkPlane.subdivide_width = 8
	var surfaceTool = SurfaceTool.new()
	surfaceTool.create_from(chunkPlane, 0)
	var arrayPlane = surfaceTool.commit()
	var dataTool = MeshDataTool.new()
	dataTool.create_from_surface(arrayPlane, 0)
	var needwater = false
	for i in range(dataTool.get_vertex_count()):
		var vertex = dataTool.get_vertex(i)
		var offsettedHeight = Vector3(vertex.x + offset.x, vertex.y, vertex.z + offset.y)
		var finalHeight = noise.get_noise_2d(offsettedHeight.x, offsettedHeight.z) * noiseAmplitude
		var subtractHeight = snoise.get_noise_2d(offsettedHeight.x, offsettedHeight.z)
		if subtractHeight < -.15:
			subtractHeight *= 2
		finalHeight -= subtractHeight
		if !needwater and finalHeight <= waterHeight:
			needwater = true
		vertex.y = finalHeight
		dataTool.set_vertex(i, vertex)
	arrayPlane.surface_remove(0)
	dataTool.commit_to_surface(arrayPlane)
	surfaceTool.begin(Mesh.PRIMITIVE_TRIANGLES)
	surfaceTool.create_from(arrayPlane, 0)
	surfaceTool.generate_normals()
	var finalMesh = surfaceTool.commit()
	return [finalMesh, needwater]

func _add_mesh_to_chunk(chunk: StaticBody, mesh: ArrayMesh):
	if chunk != null and mesh != null:
		var chunkMesh = MeshInstance.new()
		chunkMesh.mesh = mesh
		chunkMesh.name = "cmesh"
		chunk.call_deferred("add_child", chunkMesh)

func _create_mesh_instance(mesh: ArrayMesh) -> MeshInstance:
	if !isActive: return null
	var chunkMesh = MeshInstance.new()
	chunkMesh.mesh = mesh
	chunkMesh.name = "cmesh"
	return chunkMesh

var threadsToDispose = []
var removingList = []
func _unload_chunk(chunk:Spatial):
	if !chunk: return
	if !is_instance_valid(chunk): return
	if !chunk.is_inside_tree(): return
	if chunk.is_queued_for_deletion(): return
	var mesh:MeshInstance = chunk.get_node_or_null("cmesh")
	if !mesh: return
	var faces = mesh.mesh.surface_get_arrays(0)
	chunk.queue_free()
	_write_file("user://worlds/"+ScriptShared.CurrentMap+"/chunks/"+chunk.name+".chunk", faces)

var unloadMutex:Mutex = Mutex.new()
var unloadSemaphore : Semaphore = Semaphore.new()
var debugDelListLabel = Label.new()

func _add_to_unloadlist(chunk:Spatial):
	terrain.remove_child(chunk)
	queuedForDeletion.add_child(chunk)
	if removingList.find(chunk) > -1: return
	unloadMutex.lock()
	removingList.append(chunk)
	unloadMutex.unlock()
	var _isOk = unloadSemaphore.post()
	debugDelListLabel.text = "Pending Chunk Del List: "+str(removingList.size())+" max(100)"
func _unloadthread_loop(data:Array):
	var _label = data[0]
	var idx = data[1]
	var lastAction = null
	while isActive:
		_label.text = "Chunk Del Thread #"+str(idx)+": inactive (last action: "+str(lastAction)+")"
		if removingList.size() == 0: _label.text = "Chunk Del Thread #"+str(idx)+": waiting for unloadSemaphore.post()"; unloadSemaphore.wait()
		unloadMutex.lock()
		var removeChunk = removingList.pop_front()
		unloadMutex.unlock()
		debugDelListLabel.text = "Pending Chunk Del List: "+str(removingList.size())+" max(100)"
		_label.text = "Chunk Del Thread #"+str(idx)+": removing "+str(removeChunk.name)
		lastAction = removeChunk.name
		_unload_chunk(removeChunk)


var isReady = false
onready var generatorScreen = $Camera/GeneratorScreen
onready var progressText = $Camera/GeneratorScreen/ProgressT
onready var progressBar = $Camera/GeneratorScreen/ProgressB
func _pregenerate_chunks(_nil):
	var chunkCount = (ScriptShared.RenderDistance*2) *2
	#var chunkCount = 4 *2
	generatorScreen.visible = true
	var totalChunks = chunkCount*chunkCount
	progressBar.max_value = totalChunks
	progressBar.value = 0
	progressText.text = "Generating World... 0/"+str(totalChunks)+" Chunks complete."
	var index = [1]
	Engine.target_fps = 1
	Engine.iterations_per_second = 1
	var _isOk = get_tree().connect("idle_frame", self, "_pregen_log", [index, totalChunks])
	for absChunkRadX in chunkCount:
		var chunkNoX = (-(chunkCount*.5) + absChunkRadX)
		for absChunkRadZ in chunkCount:
			var chunkNoZ = (-(chunkCount*.5) + absChunkRadZ)
			_generate_chunk_mesh([Vector2(chunkNoX*16, chunkNoZ*16)])
			index[0] += 1
	get_tree().disconnect("idle_frame", self, "_pregen_log")
	Engine.target_fps = 0
	Engine.iterations_per_second = 44
	generatorScreen.visible = false
func _pregen_log(indexarr:Array, totalChunks:int):
	var index = indexarr[0]
	progressBar.value = index
	progressText.text = "Generating World... "+str(index)+"/"+str(totalChunks)+" Chunks complete."

var generating : Array = []
var genMutex: Mutex = Mutex.new()
var genSemaphore : Semaphore = Semaphore.new()
var debugGenListLabel = Label.new()

func _chunkthread_loop(data:Array):
	var _label = data[0]
	var idx = data[1]
	while isActive:
		_label.text = "Chunk Gen Thread #"+str(idx)+": inactive"
		if generating.size() == 0: var _isOk = genSemaphore.wait()
		genMutex.lock()
		var chunk2gen = generating.pop_front()
		genMutex.unlock()
		debugGenListLabel.text = "Pending Chunk Gen List: "+str(generating.size())
		_label.text = "Chunk Gen Thread #"+str(idx)+": generating "+str(chunk2gen)
		_generate_chunk_mesh([chunk2gen])
func _generate_chunkthread(offset):
	if generating.find(offset) > -1:
		return
	genMutex.lock()
	generating.append(offset)
	genMutex.unlock()
	debugGenListLabel.text = "Pending Chunk Gen List: "+str(generating.size())
	var _isOk = genSemaphore.post()

var debugFpsLabel = Label.new()
func _ready():
	var dir = Directory.new()
	dir.make_dir_recursive("user://worlds/"+ScriptShared.CurrentMap+"/chunks/")
	var _nil = rand_seed(ScriptShared.WorldSeed)
	noise.seed = ScriptShared.WorldSeed
	noise.period = 128
	noise.persistence = .25
	noise.lacunarity = 3.5
	noise.octaves = 6
	cnoise.seed = int(ScriptShared.WorldSeed*.1)
	cnoise.period = 256
	cnoise.persistence = 0
	cnoise.octaves = 4
	snoise.seed = int(ScriptShared.WorldSeed*.5)
	snoise.octaves = 1
	snoise.period = 128
	snoise.persistence = 0
	shaderMat.shader = culledVCMat
	var waterPlane = PlaneMesh.new()
	waterPlane.size = Vector2(16,16)
	waterMesh.mesh = waterPlane
	waterMesh.name = "wmesh"
	waterMesh.material_override = culledWCMat
	var pregenThread = Thread.new()
	pregenThread.start(self, "_pregenerate_chunks", null, Thread.PRIORITY_HIGH)
	while pregenThread.is_alive(): yield(get_tree(), "idle_frame")
	pregenThread.wait_to_finish()
	ScriptShared.CurrentCamera = $Camera
	var plr = plrToSpawn.instance()
	plr.translation = Vector3(0,10,0)
	call_deferred("add_child", plr)
	isReady = true
	for _gen in range(ScriptShared.GenThreads):
		var genThread = Thread.new()
		var debugInfoLabel = Label.new()
		debugInfoLabel.text = "Chunk Gen Thread #"+str(_gen)+": inactive"
		debugInfoLabel.rect_size = Vector2(512,16)
		debugInfoLabel.theme = nerdfont
		$Camera/DebugMenu/Left.call_deferred("add_child", debugInfoLabel)
		var threadStatus = genThread.start(self, "_chunkthread_loop", [debugInfoLabel, _gen])
		if threadStatus != OK: print("Failed to start ChunkGenThread: "+str(threadStatus))
		threadsToDispose.append(genThread)
	debugGenListLabel.text = "Pending Chunk Gen List: "+str(generating.size())
	debugGenListLabel.rect_size = Vector2(512,16)
	debugGenListLabel.theme = nerdfont
	$Camera/DebugMenu/Left.call_deferred("add_child", debugGenListLabel)
	for _del in range(ScriptShared.DelThreads):
		var delThread = Thread.new()
		var debugInfoLabel = Label.new()
		debugInfoLabel.text = "Chunk Del Thread #"+str(_del)+": inactive"
		debugInfoLabel.rect_size = Vector2(512,16)
		debugInfoLabel.theme = nerdfont
		$Camera/DebugMenu/Left.call_deferred("add_child", debugInfoLabel)
		var threadStatus = delThread.start(self, "_unloadthread_loop", [debugInfoLabel, _del])
		if threadStatus != OK: print("Failed to start ChunkGenThread: "+str(threadStatus))
		threadsToDispose.append(delThread)
	debugDelListLabel.text = "Pending Chunk Del List: "+str(removingList.size())+" max(100)"
	debugDelListLabel.rect_size = Vector2(512,16)
	debugDelListLabel.theme = nerdfont
	$Camera/DebugMenu/Left.call_deferred("add_child", debugDelListLabel)
	debugFpsLabel.text = "FPS: I0/P44"
	debugFpsLabel.rect_size = Vector2(512,16)
	debugFpsLabel.theme = nerdfont
	$Camera/DebugMenu/Left.call_deferred("add_child", debugFpsLabel)

func _exit_tree():
	isActive = false
	for chunk in terrain.get_children():
		_unload_chunk(chunk)
	for thread in threadsToDispose:
		thread.wait_to_finish()

func _get_all_current_chunks():
	var returned = {}
	for child in terrain.get_children():
		returned[child.name] = child
	return returned

var lastChunk = Vector2.INF
func _process(_delta):
	if !isReady: return
	var camPos = ScriptShared.CurrentCamera.global_translation
	var currentChunkX = int(camPos.x*.0625) # is equal to camPos.x/16
	var currentChunkZ = int(camPos.z*.0625)
	var currentChunkV2 = Vector2(currentChunkX, currentChunkZ)
	var ditherDist = (ScriptShared.RenderDistance-1)*16
	env.environment.fog_depth_begin = ditherDist*.8
	env.environment.fog_depth_end = ditherDist
	if currentChunkV2 == lastChunk:
		return
	var listOfExistingChunks = _get_all_current_chunks()
	var chunkCount = ScriptShared.RenderDistance *2
	for absChunkRadX in chunkCount:
		var chunkNoX = currentChunkX+(-(chunkCount*.5) + absChunkRadX)
		for absChunkRadZ in chunkCount:
			var chunkNoZ = currentChunkZ+(-(chunkCount*.5) + absChunkRadZ)
			var dictKey = "("+str(chunkNoX*16)+", "+str(chunkNoZ*16)+")"
			var currentChunk = listOfExistingChunks.get(dictKey)
			if not currentChunk: _generate_chunkthread(Vector2(chunkNoX*16, chunkNoZ*16))
			else: listOfExistingChunks.erase(dictKey)
	var unloadIndex = 0
	for chunk in listOfExistingChunks.values():
		if unloadIndex > 64: break
		if chunk.is_queued_for_deletion(): continue
		_add_to_unloadlist(chunk)
		unloadIndex += 1
	lastChunk = currentChunkV2

func _physics_process(delta):
	if !isReady: return
	sun.rotation_degrees += Vector3(1*delta,0,0)
	debugFpsLabel.text = "FPS: I"+str(Engine.get_frames_per_second())+"/P"+str(Engine.get_iterations_per_second())
