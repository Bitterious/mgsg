extends Camera

var sens = .2

func _ready():
	pass

func _input(event):
	if (event is InputEventMouseMotion or event is InputEventScreenDrag) and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		self.rotation.x += (deg2rad(event.relative.y * sens * -1))
		self.rotation.y += (deg2rad(event.relative.x * sens * -1))
		var camera_rot = self.rotation_degrees
		camera_rot.x = clamp(camera_rot.x, -89, 89)
		self.rotation_degrees = camera_rot
	elif (event is InputEventKey) and event.scancode == KEY_F3 and event.pressed:
		$DebugMenu.visible = !$DebugMenu.visible
