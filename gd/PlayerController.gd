extends Spatial

var GRAVITY = -30
var vel = Vector3()
var MAX_SPEED = 7
var JUMP_SPEED = 12
var ACCEL = 2
var dir = Vector3()
const DEACCEL= 16
const MAX_SLOPE_ANGLE = 60

var isInControl = true
var cameraOffset = -1
onready var camera  = ScriptShared.CurrentCamera
onready var kinematic = $Kinematic
onready var rotatables = $Kinematic/Rotatables

func _ready():
	ScriptShared.ControlledPlayer = self
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _process(_delta):
	camera.global_translation = kinematic.global_translation + (Vector3(0,cameraOffset,0))

func _physics_process(delta):
	rotatables.rotation.z = camera.rotation.y
	if Input.is_action_just_pressed("movement_jump") and isInControl and kinematic.is_on_floor():
			vel.y = JUMP_SPEED
	if Input.is_action_just_pressed("ui_cancel"):
		if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
			isInControl = true
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
			isInControl = false
	if isInControl:
		process_input(delta)
	else:
		dir = Vector3()
	process_movement(delta)

func process_input(_delta):
	dir = Vector3()
	var cam_xform = camera.get_global_transform()
	var input_movement_vector = Vector2()
	if Input.is_action_pressed("movement_forward"):
		input_movement_vector.y += 1
	if Input.is_action_pressed("movement_backward"):
		input_movement_vector.y -= 1
	if Input.is_action_pressed("movement_left"):
		input_movement_vector.x -= 1
	if Input.is_action_pressed("movement_right"):
		input_movement_vector.x += 1
	input_movement_vector = input_movement_vector.normalized()
	dir += -cam_xform.basis.z * input_movement_vector.y
	dir += cam_xform.basis.x * input_movement_vector.x

func process_movement(delta):
	dir.y = 0
	dir = dir.normalized()
	if !kinematic.is_on_floor():
		vel.y += delta * GRAVITY
	vel.y = clamp(vel.y, -25, 25)
	var hvel = vel
	hvel.y = 0
	var target = dir
	target *= MAX_SPEED
	var accel
	if dir.dot(hvel) > 0:
		accel = ACCEL
	else:
		accel = DEACCEL
	
	hvel = hvel.linear_interpolate(target, accel*delta)
	vel.x = hvel.x
	vel.z = hvel.z
	vel = kinematic.move_and_slide(vel, Vector3.UP, true)
